"""
A Spawner for JupyterHub that runs each user's server in a separate docker container
"""
import jupyterhub
import itertools
import json
import os
import string
import warnings
from textwrap import dedent
from concurrent.futures import ThreadPoolExecutor
from pprint import pformat

from agavepy.agave import Agave
import docker
from docker.errors import APIError
from docker.utils import kwargs_from_env
import requests
from tornado import gen

from escapism import escape
from jupyterhub.spawner import Spawner
from traitlets import (
    Dict,
    Unicode,
    Bool,
    Int,
    Any,
    default,
    observe,
)

#from .volumenamingstrategy import default_format_volume_name

_jupyterhub_xy = '%i.%i' % (jupyterhub.version_info[:2])

# TAS configuration:
# base URL for TAS API.
TAS_URL_BASE = os.environ.get('TAS_URL_BASE', 'https://tas.tacc.utexas.edu/api/v1')
TAS_ROLE_ACCT = os.environ.get('TAS_ROLE_ACCT', 'tas-jetstream')
TAS_ROLE_PASS = os.environ.get('TAS_ROLE_PASS')
class UnicodeOrFalse(Unicode):
    info_text = 'a unicode string or False'
    def validate(self, obj, value):
        if value is False:
            return value
        return super(UnicodeOrFalse, self).validate(obj, value)

import jupyterhub
_jupyterhub_xy = '%i.%i' % (jupyterhub.version_info[:2])

class DockerSpawner(Spawner):

    _executor = None
    @property
    def executor(self):
        """single global executor"""
        cls = self.__class__
        if cls._executor is None:
            cls._executor = ThreadPoolExecutor(1)
        return cls._executor

    _client = None

    @property
    def client(self):
        """single global client instance"""
        cls = self.__class__
        if cls._client is None:
            kwargs = {'version':'auto'}
            if self.tls_config:
                kwargs['tls'] = docker.tls.TLSConfig(**self.tls_config)
            kwargs.update(kwargs_from_env())
            kwargs.update(self.client_kwargs)
            client = docker.APIClient(**kwargs)
            cls._client = client
        return cls._client

    # notice when user has set the command
    # default command is that of the container,
    # but user can override it via config
    _user_set_cmd = False
    @observe('cmd')
    def _cmd_changed(self, change):
        self._user_set_cmd = True

    container_id = Unicode()

    # deprecate misleading container_ip, since
    # it is not the ip in the container,
    # but the host ip of the port forwarded to the container
    # when use_internal_ip is False
    container_ip = Unicode('127.0.0.1', config=True)
    @observe('container_ip')
    def _container_ip_deprecated(self, change):
        self.log.warning(
            "DockerSpawner.container_ip is deprecated in dockerspawner-0.9."
            "  Use DockerSpawner.host_ip to specify the host ip that is forwarded to the container"
        )
        self.host_ip = change.new

    host_ip = Unicode('127.0.0.1',
        help="""The ip address on the host on which to expose the container's port

        Typically 127.0.0.1, but can be public interfaces as well
        in cases where the Hub and/or proxy are on different machines
        from the user containers.

        Only used when use_internal_ip = False.
        """
    )

    # unlike container_ip, container_port is the internal port
    # on which the server is bound.
    container_port = Int(8888, min=1, max=65535, config=True)
    @observe('container_port')
    def _container_port_changed(self, change):
        self.log.warning(
            "DockerSpawner.container_port is deprecated in dockerspawner 0.9."
            "  Use DockerSpawner.port"
        )
        self.port = change.new

    # fix default port to 8888, used in the container
    @default('port')
    def _port_default(self):
        return 8888

    # default to listening on all-interfaces in the container
    @default('ip')
    def _ip_default(self):
        return '0.0.0.0'

    container_image = Unicode("jupyterhub/singleuser:%s" % _jupyterhub_xy, config=True)
    @observe('container_image')
    def _container_image_changed(self, change):
        self.log.warning(
            "DockerSpawner.container_image is deprecated in dockerspawner 0.9."
            "  Use DockerSpawner.image"
        )
        self.image = change.new

    image = Unicode("jupyterhub/singleuser:%s" % _jupyterhub_xy, config=True,
        help="""The image to use for single-user servers.

        This image should have the same version of jupyterhub as
        the Hub itself installed.

        If the default command of the image does not launch
        jupyterhub-singleuser, set `c.Spawner.cmd` to
        launch jupyterhub-singleuser, e.g.

        Any of the jupyter docker-stacks should work without additional config,
        as long as the version of jupyterhub in the image is compatible.
        """
    )

    container_prefix = Unicode(
        "jupyter",
        config=True,
        help=dedent(
            """
            Prefix for container names. See container_name_template for full container name for a particular
            user.
            """
        )
    )

    container_name_template = Unicode(
        "{prefix}-{username}",
        config=True,
        help=dedent(
            """
            Name of the container: with {username}, {imagename}, {prefix} replacements.
            The default container_name_template is <prefix>-<username> for backward compatibility
            """
        )
    )

    client_kwargs = Dict(
        config=True,
        help="Extra keyword arguments to pass to the docker.Client constructor.",
    )

    volumes = Dict(
        config=True,
        help=dedent(
            """
            Map from host file/directory to container (guest) file/directory
            mount point and (optionally) a mode. When specifying the
            guest mount point (bind) for the volume, you may use a
            dict or str. If a str, then the volume will default to a
            read-write (mode="rw"). With a dict, the bind is
            identified by "bind" and the "mode" may be one of "rw"
            (default), "ro" (read-only), "z" (public/shared SELinux
            volume label), and "Z" (private/unshared SELinux volume
            label).

            If format_volume_name is not set,
            default_format_volume_name is used for naming volumes.
            In this case, if you use {username} in either the host or guest
            file/directory path, it will be replaced with the current
            user's name.
            """
        )
    )

    read_only_volumes = Dict(
        config=True,
        help=dedent(
            """
            Map from host file/directory to container file/directory.
            Volumes specified here will be read-only in the container.

            If format_volume_name is not set,
            default_format_volume_name is used for naming volumes.
            In this case, if you use {username} in either the host or guest
            file/directory path, it will be replaced with the current
            user's name.
            """
        )
    )

    format_volume_name = Any(
        help="""Any callable that accepts a string template and a DockerSpawner instance as parameters in that order and returns a string.

        Reusable implementations should go in dockerspawner.VolumeNamingStrategy, tests should go in ...
        """
    ).tag(config=True)

    def default_format_volume_name(template, spawner):
        return template.format(username=spawner.user.name)

    @default('format_volume_name')
    def _get_default_format_volume_name(self):
        return default_format_volume_name

    use_docker_client_env = Bool(True, config=True,
        help="DEPRECATED. Docker env variables are always used if present.")
    @observe('use_docker_client_env')
    def _client_env_changed(self):
        self.log.warning("DockerSpawner.use_docker_client_env is deprecated and ignored."
        "  Docker environment variables are always used if defined.")
    tls_config = Dict(config=True,
        help="""Arguments to pass to docker TLS configuration.

        See docker.client.TLSConfig constructor for options.
        """
    )
    tls = tls_verify = tls_ca = tls_cert = \
    tls_key = tls_assert_hostname = Any(config=True,
        help="""DEPRECATED. Use DockerSpawner.tls_config dict to set any TLS options."""
    )
    @observe('tls', 'tls_verify', 'tls_ca', 'tls_cert', 'tls_key', 'tls_assert_hostname')
    def _tls_changed(self, change):
        self.log.warning("%s config ignored, use %s.tls_config dict to set full TLS configuration.",
            change.name, self.__class__.__name__,
        )

    remove_containers = Bool(False, config=True, help="If True, delete containers after they are stopped.")

    @property
    def will_resume(self):
        # indicate that we will resume,
        # so JupyterHub >= 0.7.1 won't cleanup our API token
        return not self.remove_containers

    extra_create_kwargs = Dict(config=True, help="Additional args to pass for container create")
    extra_start_kwargs = Dict(config=True, help="Additional args to pass for container start")
    extra_host_config = Dict(config=True, help="Additional args to create_host_config for container create")

    _container_safe_chars = set(string.ascii_letters + string.digits + '-')
    _container_escape_char = '_'

    hub_ip_connect = Unicode(
        config=True,
        help=dedent(
            """
            If set, DockerSpawner will configure the containers to use
            the specified IP to connect the hub api.  This is useful
            when the hub_api is bound to listen on all ports or is
            running inside of a container.
            """
        )
    )
    @observe('hub_ip_connect')
    def _ip_connect_changed(self, change):
        if jupyterhub.version_info >= (0, 8):
            warnings.warn(
                "DockerSpawner.hub_ip_connect is no longer needed with JupyterHub 0.8."
                "  Use JupyterHub.hub_connect_ip instead.",
                DeprecationWarning,
            )

    use_internal_ip = Bool(False,
        config=True,
        help=dedent(
            """
            Enable the usage of the internal docker ip. This is useful if you are running
            jupyterhub (as a container) and the user containers within the same docker network.
            E.g. by mounting the docker socket of the host into the jupyterhub container.
            Default is True if using a docker network, False if bridge or host networking is used.
            """
        )
    )
    @default('use_internal_ip')
    def _default_use_ip(self):
        # setting network_name to something other than bridge or host implies use_internal_ip
        if self.network_name not in {'bridge', 'host'}:
            return True
        else:
            return False

    links = Dict(
        config=True,
        help=dedent(
            """
            Specify docker link mapping to add to the container, e.g.

                links = {'jupyterhub': 'jupyterhub'}

            If the Hub is running in a Docker container,
            this can simplify routing because all traffic will be using docker hostnames.
            """
        )
    )

    network_name = Unicode(
        "bridge",
        config=True,
        help=dedent(
            """
            Run the containers on this docker network.
            If it is an internal docker network, the Hub should be on the same network,
            as internal docker IP addresses will be used.
            For bridge networking, external ports will be bound.
            """
        )
    )

    @property
    def tls_client(self):
        """A tuple consisting of the TLS client certificate and key if they
        have been provided, otherwise None.

        """
        if self.tls_cert and self.tls_key:
            return (self.tls_cert, self.tls_key)
        return None

    @property
    def volume_mount_points(self):
        """
        Volumes are declared in docker-py in two stages.  First, you declare
        all the locations where you're going to mount volumes when you call
        create_container.
        Returns a sorted list of all the values in self.volumes or
        self.read_only_volumes.
        """
        host_paths, _, _ = self.get_custom_mounts()
        return list(
            itertools.chain(
                self.volumes.values(),
                self.read_only_volumes.values(),
                ['/etc/.agpy', '/home/jupyter/.agave/current', '/home/jupyter/notebook.log'],
                host_paths,
            )
        )

    @property
    def volume_binds(self):
        """
        The second half of declaring a volume with docker-py happens when you
        actually call start().  The required format is a dict of dicts that
        looks like:

        {
            host_location: {'bind': container_location, 'ro': True}
        }
        """
        volumes = {
            key.format(username=self.user.name): {'bind': value.format(username=self.user.name), 'ro': False}
            for key, value in self.volumes.items()
        }
        ro_volumes = {
            key.format(username=self.user.name): {'bind': value.format(username=self.user.name), 'ro': True}
            for key, value in self.read_only_volumes.items()
        }
        tenant_id = os.environ.get('AGAVE_TENANT_ID')
        volumes.update(ro_volumes)
        volumes['/tokens/{}/{}/.agpy'.format(tenant_id, self.user.name)] = { 'bind': '/etc/.agpy', 'ro': False}
        volumes['/tokens/{}/{}/current'.format(tenant_id, self.user.name)] = { 'bind': '/home/jupyter/.agave/current', 'ro': False}
        volumes['/home/apim/logs/{}/{}/notebook.log'.format(tenant_id, self.user.name)] = { 'bind': '/home/jupyter/notebook.log', 'ro': False}
        host_paths, container_paths, types = self.get_custom_mounts()
        for i in range(len(host_paths)):
            ro = 'ro' in str(types[i])
            volumes[host_paths[i]] = {'bind': container_paths[i], 'ro': ro}
            self.log.info('container_path: {}; read_only: {}, types[i]: {}'.format(container_paths[i], ro, types[i]))
        return volumes

    _escaped_name = None

    @property
    def escaped_name(self):
        if self._escaped_name is None:
            self._escaped_name = escape(self.user.name,
                safe=self._container_safe_chars,
                escape_char=self._container_escape_char,
            )
        return self._escaped_name

    @property
    def container_name(self):
        escaped_container_image = self.image.replace("/", "_")
        server_name = getattr(self, 'name', '')
        d = {'username': self.escaped_name, 'imagename': escaped_container_image, 'servername': server_name,
             'prefix': self.container_prefix}
        return self.container_name_template.format(**d)

    def set_agave_access_data(self):
        """
        Returns the access token and base URL cached in the agavepy file
        :return:
        """

        token_file = os.path.join('/tokens', self.tenant_id, self.user.name, '.agpy')
        self.log.info("spawner looking for token file: {} for user: {}".format(token_file, self.user.name))
        if not os.path.exists(token_file):
            self.log.warn("dockerspawner did not find a token file at {}".format(token_file))
            self.access_token = None
            return None
        try:
            data = json.load(open(token_file))
        except ValueError:
            self.log.warn('could not ready json from token file')
            return None
        try:
            self.access_token = data[0]['token']
            self.log.info("Setting token: {}".format(self.access_token))
            self.url = data[0]['api_server']
            self.log.info("Setting url: {}".format(self.url))
            return None
        except (TypeError, KeyError):
            self.access_token = None
            self.url = None
            self.log.warn("token file did not have an access token and/or an api_server. data: {}".format(data))
        return None

    def get_projects(self):
        """
        Appends to self.host_paths, self.container_paths, and self.types corresponding
        to projects the user has access to.
        :return:
        """
        self.log.info("top of get_projects.")
        self.host_projects_root_dir = os.environ.get('HOST_PROJECTS_ROOT_DIR')
        self.container_projects_root_dir = os.environ.get('CONTAINER_PROJECTS_ROOT_DIR')
        if not self.host_projects_root_dir or not self.container_projects_root_dir:
            self.log.info("No host_projects_root_dir or container_projects_root_dir. env: {}".format(os.environ))
            return None
        if not self.access_token or not self.url:
            self.log.info("no access_token or url")
            return None
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        url = '{}/projects/v2/'.format(self.url)
        try:
            rsp = requests.get(url, headers=headers)
        except Exception as e:
            self.log.warn("Got exception calling /projects: {}".format(e))
            return None
        try:
            data = rsp.json()
        except ValueError as e:
            self.log.warn("Did not get JSON from /projects. Exception: {}".format(e))
            self.log.warn("Full response from service: {}".format(rsp))
            self.log.warn("url used: {}".format(url))
            return None
        projects = data.get('projects')
        self.log.info("service returned projects: {}".format(projects))
        try:
            self.log.info("Found {} projects".format(len(projects)))
        except TypeError:
            self.log.error("Projects data has no length.")
            self.log.info("response: {}, data: {}".format(rsp, data))
            return None
        for p in projects:
            uuid = p.get('uuid')
            if not uuid:
                self.log.warn("Did not get a uuid for a project: {}".format(p))
                continue
            project_id = p.get('value').get('projectId')
            if not project_id:
                self.log.warn("Did not get a projectId for a project: {}".format(p))
                continue
            self.host_paths.append('{}/{}'.format(self.host_projects_root_dir,
                                                  uuid)),
            self.container_paths.append('{}/{}'.format(self.container_projects_root_dir,
                                                       project_id))
            # projects are alwAYS read-write
            self.types.append('rw')

    def homedir_exists(self):
        """
        Check to see if a user's host path exists in the Agave. We use the configuredstorage system to make
        this check.
        :param host_path:
        :return:
        """
        self.log.info("top of check_if_homedir_exists.")
        self.agave_storage_system = os.environ.get('AGAVE_STORAGE_SYSTEM')
        if not self.agave_storage_system:
            self.log.info("No agave_storage_system inside homedir check. env: {}".format(os.environ))
            return False
        if not self.access_token or not self.url:
            self.log.info("no access_token or url inside homedir check.")
            return False
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        url = '{}/files/v2/listings/system/{}/{}'.format(self.url, self.agave_storage_system, self.user.name)
        try:
            rsp = requests.get(url, headers=headers)
        except Exception as e:
            self.log.warn("Got exception calling /files: {}".format(e))
            return False
        if not rsp.status_code == 200:
            self.log.warn("Did not get a 200 from /files. {}".format(rsp.status_code))
            return False
        try:
            data = rsp.json()
        except ValueError as e:
            self.log.warn("Did not get JSON from /files. Exception: {}".format(e))
            self.log.warn("Full response from service: {}".format(rsp))
            self.log.warn("url used: {}".format(url))
            return False
        self.log.info("Found home dir. Mounting it.")
        return True


    def get_custom_mounts(self):
        """
        Return data corresponding to custom mounts read from the file /volume_mounts.
        Returns three lists: host_paths, container_paths, types
        where each element of types is either 'ro' or 'rw'
        """
        self.log.info("enter get_custom_mounts")
        self.tenant_id = os.environ.get('AGAVE_TENANT_ID')
        self.host_paths = []
        self.container_paths = []
        self.types = []
        self.set_agave_access_data()
        self.get_projects()
        # we need tas data to resolve tokens (e.g. tas_homeDirectory) in the mounts file
        self.get_tas_data()

        check_homedir = os.environ.get('CHECK_HOMEDIR_EXISTS', True)
        self.log.info("check_homedir: {}".format(check_homedir))
        if hasattr(check_homedir, 'upper') and check_homedir.upper() == 'FALSE':
            check_homedir = False
        with open('/volume_mounts', 'r') as f:
            for line in f.readlines():
                if not ':' in line:
                    self.log.error("There was an invalid line in the volume_mounts: {}".format(line))
                    continue
                host_path, container_path, typ = line.split(':')
                # if '{username}' in host_path and check_homedir:
                #     if not self.homedir_exists():
                #         self.log.info("Skipping the home dir:{} because we got false from homedir_exists".format(line))
                #         continue
                host_path = host_path.replace('{username}', self.user.name)
                host_path = host_path.replace('{tenant_id}', self.tenant_id)
                container_path = container_path.replace('{username}', self.user.name)
                container_path = container_path.replace('{tenant_id}', self.tenant_id)
                if 'tas_homeDirectory' in host_path or 'tas_homeDirectory' in container_path:
                    # without tas data we cannot resolve this mount:
                    if not hasattr(self, 'tas_homedir') or not self.tas_homedir:
                        self.log.error("volume_mounts had: {} but self did not have tas_homedir. self:{}".format(line, self))
                        continue
                    host_path = host_path.replace('{tas_homeDirectory}', self.tas_homedir)
                    container_path = container_path.replace('{tas_homeDirectory}', self.tas_homedir)
                self.host_paths.append(host_path)
                self.container_paths.append(container_path)
                self.types.append(typ)
        self.log.info("returning the following host_paths: {} container_path: {} and types: {}".format(
            self.host_paths, self.container_paths, self.types))
        return self.host_paths, self.container_paths, self.types

    def get_tas_data(self):
        """Get the TACC uid, gid and homedir for this user from the TAS API."""
        self.log.info("Top of get_tas_data")
        if not TAS_ROLE_ACCT:
            self.log.error("No TAS_ROLE_ACCT configured. Aborting.")
            return
        if not TAS_ROLE_PASS:
            self.log.error("No TAS_ROLE_PASS configured. Aborting.")
            return
        url = '{}/users/username/{}'.format(TAS_URL_BASE, self.user.name)
        headers = {'Content-type': 'application/json',
                   'Accept': 'application/json'
                   }
        try:
            rsp = requests.get(url,
                               headers=headers,
                               auth=requests.auth.HTTPBasicAuth(TAS_ROLE_ACCT, TAS_ROLE_PASS))
        except Exception as e:
            self.log.error("Got an exception from TAS API. "
                           "Exception: {}. url: {}. TAS_ROLE_ACCT: {}".format(e, url, TAS_ROLE_ACCT))
            return
        try:
            data = rsp.json()
        except Exception as e:
            self.log.error("Did not get JSON from TAS API. rsp: {}"
                           "Exception: {}. url: {}. TAS_ROLE_ACCT: {}".format(rsp, e, url, TAS_ROLE_ACCT))
            return
        try:
            self.tas_uid = data['result']['uid']
            self.tas_homedir = data['result']['homeDirectory']
        except Exception as e:
            self.log.error("Did not get attributes from TAS API. rsp: {}"
                           "Exception: {}. url: {}. TAS_ROLE_ACCT: {}".format(rsp, e, url, TAS_ROLE_ACCT))
            return

        # first look for an "extended profile" record in agave metadata. such a record might have the
        # gid to use for this user.
        self.tas_gid = None
        if self.access_token and self.url:
            ag = Agave(api_server=self.url, token=self.access_token)
            meta_name = 'profile.{}.{}'.format(self.tenant_id, self.user.name)
            q = "{'name': '" + meta_name + "'}"
            self.log.info("using query: {}".format(q))
            try:
                rsp = ag.meta.listMetadata(q=q)
            except Exception as e:
                self.log.error("Got an exception trying to retrieve the extended profile. Exception: {}".format(e))
            try:
                self.tas_gid = rsp[0].value['posix_gid']
            except IndexError:
                self.tas_gid = None
            except Exception as e:
                self.log.error("Got an exception trying to retrieve the gid from the extended profile. Exception: {}".format(e))
        # if the instance has a configured TAS_GID to use we will use that; otherwise,
        # we fall back on using the user's uid as the gid, which is (almost) always safe)
        if not self.tas_gid:
            self.tas_gid = os.environ.get('TAS_GID', self.tas_uid)
        self.log.info("Setting the following TAS data: uid:{} gid:{} homedir:{}".format(self.tas_uid,
                                                                                        self.tas_gid,
                                                                                        self.tas_homedir))

    def load_state(self, state):
        super(DockerSpawner, self).load_state(state)
        self.container_id = state.get('container_id', '')

    def get_state(self):
        state = super(DockerSpawner, self).get_state()
        if self.container_id:
            state['container_id'] = self.container_id
        return state

    def _public_hub_api_url(self):
        proto, path = self.hub.api_url.split('://', 1)
        ip, rest = path.split(':', 1)
        return '{proto}://{ip}:{rest}'.format(
            proto = proto,
            ip = self.hub_ip_connect,
            rest = rest
        )

    def _env_keep_default(self):
        """Don't inherit any env from the parent process"""
        return []

    def get_args(self):
        args = super().get_args()
        if self.hub_ip_connect:
            # JupyterHub 0.7 specifies --hub-api-url
            # on the command-line, which is hard to update
            for idx, arg in enumerate(list(args)):
                if arg.startswith('--hub-api-url='):
                    args.pop(idx)
                    break
            args.append('--hub-api-url=%s' % self._public_hub_api_url())
        return args

    def _docker(self, method, *args, **kwargs):
        """wrapper for calling docker methods

        to be passed to ThreadPoolExecutor
        """
        m = getattr(self.client, method)
        return m(*args, **kwargs)

    def docker(self, method, *args, **kwargs):
        """Call a docker method in a background thread

        returns a Future
        """
        return self.executor.submit(self._docker, method, *args, **kwargs)

    @gen.coroutine
    def poll(self):
        """Check for my id in `docker ps`"""
        container = yield self.get_container()
        if not container:
            self.log.warn("container not found")
            return 0

        container_state = container['State']
        self.log.debug(
            "Container %s status: %s",
            self.container_id[:7],
            pformat(container_state),
        )

        if container_state["Running"]:
            return None
        else:
            return (
                "ExitCode={ExitCode}, "
                "Error='{Error}', "
                "FinishedAt={FinishedAt}".format(**container_state)
            )

    @gen.coroutine
    def get_container(self):
        self.log.debug("Getting container '%s'", self.container_name)
        try:
            container = yield self.docker(
                'inspect_container', self.container_name
            )
            self.container_id = container['Id']
        except APIError as e:
            if e.response.status_code == 404:
                self.log.info("Container '%s' is gone", self.container_name)
                container = None
                # my container is gone, forget my id
                self.container_id = ''
            elif e.response.status_code == 500:
                self.log.info("Container '%s' is on unhealthy node", self.container_name)
                container = None
                # my container is unhealthy, forget my id
                self.container_id = ''
            else:
                raise
        return container

    @gen.coroutine
    def start(self, image=None, extra_create_kwargs=None,
        extra_start_kwargs=None, extra_host_config=None):
        """Start the single-user server in a docker container. You can override
        the default parameters passed to `create_container` through the
        `extra_create_kwargs` dictionary and passed to `start` through the
        `extra_start_kwargs` dictionary.  You can also override the
        'host_config' parameter passed to `create_container` through the
        `extra_host_config` dictionary.

        Per-instance `extra_create_kwargs`, `extra_start_kwargs`, and
        `extra_host_config` take precedence over their global counterparts.

        """
        # self.log.info("top of dockerspawner.start()")
        # self.log.info("self.user_options = {}".format(self.user_options))
        # if self.user_options.get('image') == ['lab']:
        #     self.cmd = ['jupyter-labhub']

        container = yield self.get_container()
        if container and self.remove_containers:
            self.log.warning(
                "Removing container that should have been cleaned up: %s (id: %s)",
                self.container_name, self.container_id[:7])
            # remove the container, as well as any associated volumes
            yield self.docker('remove_container', self.container_id, v=True)
            container = None

        if container is None:
            image = image or self.image
            if self._user_set_cmd:
                cmd = self.cmd
            else:
                image_info = yield self.docker('inspect_image', image)
                cmd = image_info['Config']['Cmd']
            cmd = cmd + self.get_args()

            # build the dictionary of keyword arguments for create_container
            create_kwargs = dict(
                image=image,
                environment=self.get_env(),
                volumes=self.volume_mount_points,
                name=self.container_name,
                command=cmd,
            )

            # ensure internal port is exposed
            create_kwargs['ports'] = {'%i/tcp' % self.port: None}

            create_kwargs.update(self.extra_create_kwargs)
            if extra_create_kwargs:
                create_kwargs.update(extra_create_kwargs)

            # build the dictionary of keyword arguments for host_config
            host_config = dict(binds=self.volume_binds, links=self.links)

            if hasattr(self, 'mem_limit') and self.mem_limit is not None:
                # If jupyterhub version > 0.7, mem_limit is a traitlet that can
                # be directly configured. If so, use it to set mem_limit.
                # this will still be overriden by extra_host_config
                host_config['mem_limit'] = self.mem_limit

            if not self.use_internal_ip:
                host_config['port_bindings'] = {self.port: (self.host_ip,)}
            host_config.update(self.extra_host_config)
            host_config.setdefault('network_mode', self.network_name)

            if extra_host_config:
                host_config.update(extra_host_config)

            self.log.debug("Starting host with config: %s", host_config)

            host_config = self.client.create_host_config(**host_config)
            create_kwargs.setdefault('host_config', {}).update(host_config)

            # add the tas uid and gid if the attributes are defined:
            if hasattr(self, 'tas_uid') \
                and hasattr(self, 'tas_gid') \
                and self.tas_uid \
                and self.tas_gid:
                create_kwargs['user'] = '{}:{}'.format(self.tas_uid, self.tas_gid)

            # create the container
            resp = yield self.docker('create_container', **create_kwargs)
            self.container_id = resp['Id']
            self.log.info(
                "Created container '%s' (id: %s) from image %s",
                self.container_name, self.container_id[:7], image)

        else:
            self.log.info(
                "Found existing container '%s' (id: %s)",
                self.container_name, self.container_id[:7])
            # Handle re-using API token.
            # Get the API token from the environment variables
            # of the running container:
            for line in container['Config']['Env']:
                if line.startswith(('JPY_API_TOKEN=', 'JUPYTERHUB_API_TOKEN=')):
                    self.api_token = line.split('=', 1)[1]
                    break

        # TODO: handle unpause
        self.log.info(
            "Starting container '%s' (id: %s)",
            self.container_name, self.container_id[:7])

        # build the dictionary of keyword arguments for start
        start_kwargs = {}
        start_kwargs.update(self.extra_start_kwargs)
        if extra_start_kwargs:
            start_kwargs.update(extra_start_kwargs)

        # start the container
        yield self.docker('start', self.container_id, **start_kwargs)

        ip, port = yield self.get_ip_and_port()
        if jupyterhub.version_info < (0,7):
            # store on user for pre-jupyterhub-0.7:
            self.user.server.ip = ip
            self.user.server.port = port
        # jupyterhub 0.7 prefers returning ip, port:
        return (ip, port)

    @gen.coroutine
    def get_ip_and_port(self):
        """Queries Docker daemon for container's IP and port.

        If you are using network_mode=host, you will need to override
        this method as follows::

            @gen.coroutine
            def get_ip_and_port(self):
                return self.host_ip, self.port

        You will need to make sure host_ip and port
        are correct, which depends on the route to the container
        and the port it opens.
        """

        if self.use_internal_ip:
            resp = yield self.docker('inspect_container', self.container_id)
            network_settings = resp['NetworkSettings']
            if 'Networks' in network_settings:
                ip = self.get_network_ip(network_settings)
            else:  # Fallback for old versions of docker (<1.9) without network management
                ip = network_settings['IPAddress']
            port = self.port
        else:
            resp = yield self.docker('port', self.container_id, self.port)
            if resp is None:
                raise RuntimeError("Failed to get port info for %s" % self.container_id)
            ip = resp[0]['HostIp']
            port = int(resp[0]['HostPort'])
        return ip, port

    def get_network_ip(self, network_settings):
        networks = network_settings['Networks']
        if self.network_name not in networks:
            raise Exception(
                "Unknown docker network '{network}'."
                " Did you create it with `docker network create <name>`?".format(
                    network=self.network_name
                )
            )
        network = networks[self.network_name]
        ip = network['IPAddress']
        return ip

    @gen.coroutine
    def stop(self, now=False):
        """Stop the container

        Consider using pause/unpause when docker-py adds support
        """
        self.log.info(
            "Stopping container %s (id: %s)",
            self.container_name, self.container_id[:7])
        yield self.docker('stop', self.container_id)

        if self.remove_containers:
            self.log.info(
                "Removing container %s (id: %s)",
                self.container_name, self.container_id[:7])
            # remove the container, as well as any associated volumes
            yield self.docker('remove_container', self.container_id, v=True)

        self.clear_state()

    def _volumes_to_binds(self, volumes, binds, mode='rw'):
        """Extract the volume mount points from volumes property.

        Returns a dict of dict entries of the form::

            {'/host/dir': {'bind': '/guest/dir': 'mode': 'rw'}}
        """
        def _fmt(v):
            return self.format_volume_name(v, self)

        for k, v in volumes.items():
            m = mode
            if isinstance(v, dict):
                if 'mode' in v:
                    m = v['mode']
                v = v['bind']
            binds[_fmt(k)] = {'bind': _fmt(v), 'mode': m}
        return binds

