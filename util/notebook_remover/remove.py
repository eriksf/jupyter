# Python script to remove a user's notebooks from a TACC-hosted JupyterHub instance.

# There are three ways to run this script:
# 1). Pass the container_id to remove directly as an env var (container_id).
# 2). Pass a username through the env var username; this script will remove all notebooks
#     associated with that user.
# 3). Pass nothing; this script will simply print a list of all containers on the host.

import os
import subprocess

def remove_cid(cid):
    """
    Remove the container with id, `cid`.
    :param cid:
    :return:
    """
    print("container_id {} passed; attempting to remove it directly.".format(cid))
    cmd = "docker -H 172.17.0.1:4000 rm -f {}".format(cid)
    p = subprocess.check_output([cmd], shell=True)
    lines = iter(p.splitlines())
    for l in lines:
        print(l)

def remove_container_name(name):
    """
    Remove a container with a specific name, `name`
    :param name: the name of the container to remove
    :return:

    """
    cmd = "docker -H 172.17.0.1:4000 rm -f {}".format(name)
    p = subprocess.check_output([cmd], shell=True)
    lines = iter(p.splitlines())
    for l in lines:
        print(l)


def get_username(l):
    strs = l.split('jupyter-')
    # for clusters where the worker nodes contian the name "jupyter-work", the string `l`
    # has the form jupyter-work03/jupyter-<username> so the username is the 3rd entry;
    if 'jupyter-work' in l:
        if len(strs) < 3:
            return None
        return strs[2]
    # otherwise, we want the 2nd entry:
    else:
        if len(strs) < 2:
            return None
        return strs[1]

def remove_username(username):
    """
    Remove all containers with the associated username.
    :param username:
    :return:
    """
    # a list of conatiner names to remove.
    names = []
    # this command gets the complete list of jupyter notebooks using the docker CLI
    # and
    cmd = "docker -H 172.17.0.1:4000 ps -a | awk '{print $NF}'"
    p = subprocess.check_output([cmd], shell=True)
    lines = iter(p.splitlines())
    for l in lines:
        l_str = l.decode('utf-8')
        name = get_username(l_str)
        if name == username:
            names.append(l_str)
    print("Found {} containers associated with user {}".format(len(names), username))
    print("Removing the following containers:")
    for n in names:
        print(n)
        remove_container_name(n)


def main():
    # the container id to remove
    cid = os.environ.get('container_id')
    # the user name of the notebooks to remove
    username = os.environ.get('username')
    # if a container id is passed, just remove it:
    if cid:
        remove_cid(cid)
    # otherwise, if a username is passed, look up all containers associated with that user
    elif username:
        remove_username(username)
    # otherwise, print all containers:
    else:
        print("No container_id or username passed; printing all container.\n")
        cmd = "docker -H 172.17.0.1:4000 ps -a"
        p = subprocess.check_output([cmd], shell=True)
        lines = iter(p.splitlines())
        for l in lines:
            print(l)


if __name__ == '__main__':
    main()