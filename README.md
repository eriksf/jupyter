
# Agave JupyterHub and Notebooks #

This is the main repository and project home for efforts aimed at integrating Jupyter notebooks with the Agave platform,
and, in particular, the Agave JupterHub. Integration includes:

* authorization -- an Agave OAuth based authenticator.
* notebook spawning -- custom spawner processing.
* notebook images -- custom images with Agave tools.

See project Jupyter (http://jupyter.org/) and the JupyterHub project (https://github.com/jupyterhub/jupyterhub) for
background.


## JupyterHub Versioning ##

The TACC JupyterHub version scheme follows the pattern: `taccsciapps/jupyterhub:a.b.ch-X.Y.Z`
where `a.b.c` is the `jupyterhub/jupyterhub` version used and `X.Y.Z` is the version of the TACC customizations for that

The first releases were based on a very early version of the `jupyter/jupyterhub` image (pre 0.4, no longer available
on docker hub), and thus are labeled with "alpha".

For example `taccsciapps/jupyterhub:alpha-1.0.0` represents the first release of the TACC jupyterhub based on the
very early jupyterhub release and similarly `taccsciapps/jupyterhub:0.7.1h-1.0.0` will represent the first release
of the TACC jupyterhub based on the 0.7.1 jupyterhub release.
