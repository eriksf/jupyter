Image: taccsciapps/jupyteruser-ds

Description: The user notebook image for the TACC DesignSage JupyterHub.

Depends:
 - taccsciapps/jupyteruser-base:0.1.1

Creates:
 - /etc/jupyteruser-ds-release --  Access version from inside container

Kernels:
 - Bash
 - Python2
 - Python3
 - R

Version: 1.2.0rc11
