# Change Log for taccsciapps/jupyteruser-ds
All notable changes to this project will be documented in this file.

## 1.1.0 - 2017-02-28
### Added
- Added geoclaw/clawpack software suite.

### Changed
- No change.

### Removed
- No change.


## 1.0.0 - 2017-02-28: Initial release.
### Added
- No change.

### Changed
- No change.

### Removed
- No change.


