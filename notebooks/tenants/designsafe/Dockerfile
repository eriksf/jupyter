# ---------------------------------
# Image: taccsciapps/jupyteruser-ds
# ---------------------------------
# The user notebook image for the TACC DesignSage JupyterHub.
#
# ------------------
# Building the image
# ------------------
# To build this image, simply change into this directory and issue:
# docker build -t taccsciapps/jupyteruser-ds:<some_tag> .
#
# Tags follow semantic versioning based on the CHANGELOG. When building new versions, always start with a release
# candidate for the image tag; for example:
# docker build -t taccsciapps/jupyteruser-ds:1.1.0-rc1 .
#
# ---------------
# Running Locally
# ---------------
# After building a release candidate, run it locally to test out your changes.
# This image is built to run on the JupyterHub. In order to run it locally, you need to provide a config file and a
# separate command. For example, change into this directory and run:
# docker run --rm -p 8888:8888 -v $(pwd)/jupyter-notebook-localconf.py:/home/jupyter/.jupyter/jupyter_notebook_config.py taccsciapps/jupyteruser-ds start-notebook.sh
# Note than an example of a jupyter-notebook-localconf.py file is provided in this directory.

FROM taccsciapps/jupyteruser-base:0.1.2

USER root
###################################################################
# record version in /etc - image_version is injected by build_jupyteruser.sh
###################################################################
ARG image_version=test
RUN echo "${image_version}" > /etc/jupyteruser-ds-release
RUN chmod a+r /etc/jupyteruser-base-release
###################################################################

# -------
# DS Apps
# -------

# opensees
COPY ./OpenSees-deb-2.5.0.6482 /usr/local/bin/OpenSees
RUN chmod 755 /usr/local/bin/OpenSees

# obspy
RUN apt-get update && apt-get install -y libxml2 libxml2-dev libxslt1-dev
RUN conda install --quiet --yes SIP qt pyqt lxml obspy
RUN conda install --quiet --yes -n python2 SIP qt pyqt lxml obspy
RUN conda clean -tipsy
#RUN apt-get install -y libxml2 libxml2-dev libxslt1-dev python-lxml python-numexpr
RUN echo "allowed_users=anybody" > /etc/X11/Xwrapper.config

# -----------
# GeoClaw / Clawpack - conda not recommended
# -----------
RUN pip install --no-cache-dir --src=/home/jupyter/util/clawpack_src -e git+https://github.com/clawpack/clawpack.git@v5.4.1#egg=clawpack-v5.4.1
RUN pip2 install --no-cache-dir --src=/home/jupyter/util/clawpack_src -e git+https://github.com/clawpack/clawpack.git@v5.4.1#egg=clawpack-v5.4.1
USER jupyter
ENV CLAW=/home/jupyter/util/clawpack_src/clawpack-v5.4.1
ENV FC=gfortran


USER root
# Install extra packages
RUN conda install -c conda-forge cartopy
RUN pip install pymongo
RUN pip install fiona
# RUN conda install h5py
# RUN git clone https://github.com/Unidata/netcdf4-python.git && cd netcdf4-python && python setup.py build && python setup.py install
RUN pip install simplekml
RUN pip install netCDF4
RUN R -e "install.packages('geotech', repos = 'http://cran.us.r-project.org')"
RUN pip2 install pymongo==3.6.1

# Opensees
RUN apt-get -y update && \
        apt-get -y install \
        subversion emacs make tcl8.6 tcl8.6-dev \
        gcc g++ gfortran mpich vim && \
        apt-get clean

# examples
USER jupyter
RUN mkdir -p /home/jupyter/util
ADD agavepy_example.ipynb /home/jupyter/util/agavepy_example.ipynb
ADD opensees-submit-example.ipynb /home/jupyter/util/opensees-submit-example.ipynb

# startx and README
RUN startx &
RUN echo "Welcome to the DesignSafe-ci JupyterHub terminal. Your DesignSafe-ci home directory is in 'mydata.' Any files you wish to save between sessions must be within that directory. OpenSees is availavble to run interactively here. " > /home/jupyter/README.txt
RUN echo "0949f6ce05b1" > /home/jupyter/.imageID
COPY rbnb.jar /home/jupyter/util/rbnb.jar

# ensure jupyter user owns its directory
USER root

RUN mkdir /home/jupyter/public

RUN ln -s /home/jupyter/MyProjects /home/jupyter/projects
RUN ln -s /home/jupyter/MyData /home/jupyter/mydata
RUN ln -s /home/jupyter/CommunityData /home/jupyter/community
RUN ln -s /home/jupyter/NEES /home/jupyter/public/nees


#RUN ln -rs  /home/jupyter/published/nees/ /home/jupyter/public/nees

#RUN mkdir /home/jupyter/published && mkdir /home/jupyter/published/nees/* && ln -sr /home/jupyter/public/nees/* /home/jupyter/published/nees
RUN chown -R jupyter:G-816877 /home/jupyter

USER jupyter
