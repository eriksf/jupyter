Image: taccsciapps/jupyteruser-base

Description: The base jupyter notebook image for all tenants within TACC JupyterHub.

Creates:
 - /etc/jupyteruser-base-release --  Access version from inside container

Kernels:
 - Bash
 - Python2
 - Python3
 - R
